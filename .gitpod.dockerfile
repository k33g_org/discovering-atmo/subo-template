FROM gitpod/workspace-full

USER gitpod

# Tools
RUN brew tap suborbital/subo && \
    brew install subo && \
    brew install exa && \
    brew install bat && \
    brew install httpie && \
    brew install hey

